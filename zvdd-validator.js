/*
	JavaScript für ZVDD Validator.
	2013 Sven-S. Porst, SUB Göttingen <porst@sub.uni-goettingen.de>
*/

/*
	Anzeige bei Klick eines Radio Buttons anpassen.
	* Eltern-<li> des aktiven Buttons und Container des aktiven Formularelements
		mit .current markieren und andere .current entfernen
*/
function selectItem (event) {
	var currentItem = event.target;
	var currentID = currentItem.id;
	var currentName = currentID.split('-')[1];
	jQuery('.validate-menu li').removeClass('current');
	jQuery(currentItem).parent().addClass('current');
	jQuery('.form-item').removeClass('current');
	jQuery('#input-' + currentName).parent().addClass('current');
}


function textChange (event) {
	var string = jQuery(event.target).val();
	var XML = parseXML(string);
}


/*
	Aufruf beim Absenden des Formulars.
	* XML parsen und wieder serialisieren
	** … dies geschieht nur, um im Ergebnis gleichartige Anführungszeichen zu haben,
		und so nicht einem PHP »Bug«, der das Parsen des XML verhindert zum Opfer zu fallen.
	** https://bugs.php.net/bug.php?id=64137 (2013, PHP 5.4.11)
	** https://bugs.php.net/bug.php?id=35247 (2005, PHP 5.0.5)
*/
function submitForm() {
	var result = (jQuery('form ul :checked').val() !== 'xml');
	
	// parse & re-serialise XML to keep PHP happy
	var jTextarea = jQuery('form textarea');
	var XMLString = jTextarea.val();
	var XML = parseXML(XMLString);
	if (XML) {
		var newXMLString = serialiseXML(XML);
		jTextarea.val(newXMLString);
		result = true;
	}

	return result;
}

function parseXML (XMLString) {
	var XML;
	
	try {
		XML = jQuery.parseXML(XMLString);
		jQuery('.validationError').addClass('hidden');
	}
	catch (error) {
		jQuery('.validationError').removeClass('hidden');
	}

	return XML;
}



/*
	Helferfunktion zur Serialisierung des XML als string.
	Sowohl in normalen Browsern als auch in IE.
*/
function serialiseXML(XML) {
	var XMLString;
	if (window.ActiveXObject) {
		// IE
		XMLString = XML.xml;
	}
	else {
		// Web browsers
		XMLString = (new XMLSerializer()).serializeToString(XML);
	}
	return XMLString;
}
