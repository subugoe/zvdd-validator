<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:sch="http://www.ascc.net/xml/schematron"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:iso="http://purl.oclc.org/dsdl/schematron"
                xmlns:mets="http://www.loc.gov/METS/"
                xmlns:mods="http://www.loc.gov/mods/v3"
                xmlns:madsrdf="http://www.loc.gov/mads/rdf/v1#"
                mets:dummy-for-xmlns=""
                mods:dummy-for-xmlns=""
                madsrdf:dummy-for-xmlns=""
                version="1.0"><!--Implementers: please note that overriding process-prolog or process-root is 
    the preferred method for meta-stylesheets to use where possible. -->
<xsl:param name="archiveDirParameter"/>
   <xsl:param name="archiveNameParameter"/>
   <xsl:param name="fileNameParameter"/>
   <xsl:param name="fileDirParameter"/>

   <!--PHASES-->

<xsl:param name="currentXSLFolder"/>


<!--PROLOG-->
<xsl:output xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:schold="http://www.ascc.net/xml/schematron"
               xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
               method="xml"
               omit-xml-declaration="no"
               standalone="yes"
               indent="yes"/>

   <!--KEYS-->


<!--DEFAULT RULES-->


<!--MODE: SCHEMATRON-SELECT-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->
<xsl:template match="*" mode="schematron-select-full-path">
      <xsl:apply-templates select="." mode="schematron-get-full-path"/>
   </xsl:template>

   <!--MODE: SCHEMATRON-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->
<xsl:template match="*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">
            <xsl:value-of select="name()"/>
            <xsl:variable name="p_1" select="1+    count(preceding-sibling::*[name()=name(current())])"/>
            <xsl:if test="$p_1&gt;1 or following-sibling::*[name()=name(current())]">[<xsl:value-of select="$p_1"/>]</xsl:if>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>*[local-name()='</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>' and namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
            <xsl:variable name="p_2"
                          select="1+   count(preceding-sibling::*[local-name()=local-name(current())])"/>
            <xsl:if test="$p_2&gt;1 or following-sibling::*[local-name()=local-name(current())]">[<xsl:value-of select="$p_2"/>]</xsl:if>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="@*" mode="schematron-get-full-path">
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">@<xsl:value-of select="name()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>@*[local-name()='</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>' and namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!--MODE: SCHEMATRON-FULL-PATH-2-->
<!--This mode can be used to generate prefixed XPath for humans-->
<xsl:template match="node() | @*" mode="schematron-get-full-path-2">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="preceding-sibling::*[name(.)=name(current())]">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template>

   <!--MODE: GENERATE-ID-FROM-PATH -->
<xsl:template match="/" mode="generate-id-from-path"/>
   <xsl:template match="text()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.text-', 1+count(preceding-sibling::text()), '-')"/>
   </xsl:template>
   <xsl:template match="comment()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.comment-', 1+count(preceding-sibling::comment()), '-')"/>
   </xsl:template>
   <xsl:template match="processing-instruction()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.processing-instruction-', 1+count(preceding-sibling::processing-instruction()), '-')"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.@', name())"/>
   </xsl:template>
   <xsl:template match="*" mode="generate-id-from-path" priority="-0.5">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:text>.</xsl:text>
      <xsl:value-of select="concat('.',name(),'-',1+count(preceding-sibling::*[name()=name(current())]),'-')"/>
   </xsl:template>
   <!--MODE: SCHEMATRON-FULL-PATH-3-->
<!--This mode can be used to generate prefixed XPath for humans 
	(Top-level element has index)-->
<xsl:template match="node() | @*" mode="schematron-get-full-path-3">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="parent::*">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template>

   <!--MODE: GENERATE-ID-2 -->
<xsl:template match="/" mode="generate-id-2">U</xsl:template>
   <xsl:template match="*" mode="generate-id-2" priority="2">
      <xsl:text>U</xsl:text>
      <xsl:number level="multiple" count="*"/>
   </xsl:template>
   <xsl:template match="node()" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>n</xsl:text>
      <xsl:number count="node()"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="string-length(local-name(.))"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="translate(name(),':','.')"/>
   </xsl:template>
   <!--Strip characters--><xsl:template match="text()" priority="-1"/>

   <!--SCHEMA METADATA-->
<xsl:template match="/">
      <svrl:schematron-output xmlns:xs="http://www.w3.org/2001/XMLSchema"
                              xmlns:schold="http://www.ascc.net/xml/schematron"
                              xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                              title="Schematron Validatierung der MODS AP 2.0 Metadaten "
                              schemaVersion="">
         <xsl:comment>
            <xsl:value-of select="$archiveDirParameter"/>   
		 <xsl:value-of select="$archiveNameParameter"/>  
		 <xsl:value-of select="$fileNameParameter"/>  
		 <xsl:value-of select="$fileDirParameter"/>
         </xsl:comment>
         <svrl:ns-prefix-in-attribute-values uri="http://www.loc.gov/METS/" prefix="mets"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.loc.gov/mods/v3" prefix="mods"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.loc.gov/mads/rdf/v1#" prefix="madsrdf"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">titleInfo</xsl:attribute>
            <xsl:attribute name="name">titleInfo</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M4"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M5"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">Name</xsl:attribute>
            <xsl:attribute name="name">Name</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M6"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M7"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M8"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M9"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M10"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M11"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M12"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M13"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M14"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">originInfo_Wurzelstrukturelement</xsl:attribute>
            <xsl:attribute name="name">originInfo_Wurzelstrukturelement</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M15"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M16"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">placeTerm</xsl:attribute>
            <xsl:attribute name="name">placeTerm</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M17"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M18"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M19"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M20"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M21"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M22"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M23"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M24"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M25"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M26"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">language</xsl:attribute>
            <xsl:attribute name="name">language</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M27"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M28"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M29"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">physicalDescription</xsl:attribute>
            <xsl:attribute name="name">physicalDescription</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M30"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M31"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">Anmerkung</xsl:attribute>
            <xsl:attribute name="name">Anmerkung</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M32"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M33"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">subject</xsl:attribute>
            <xsl:attribute name="name">subject</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M34"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">relatedItem</xsl:attribute>
            <xsl:attribute name="name">relatedItem</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M35"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M36"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M37"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">Teile</xsl:attribute>
            <xsl:attribute name="name">Teile</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M38"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M39"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">Wurzelstrukturelement_recordInfo</xsl:attribute>
            <xsl:attribute name="name">Wurzelstrukturelement_recordInfo</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M40"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">SatzID</xsl:attribute>
            <xsl:attribute name="name">SatzID</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M41"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">identifier</xsl:attribute>
            <xsl:attribute name="name">identifier</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M42"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">location</xsl:attribute>
            <xsl:attribute name="name">location</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M43"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M44"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M45"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">emptyElements</xsl:attribute>
            <xsl:attribute name="name">emptyElements</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M46"/>
         <svrl:active-pattern>
            <xsl:attribute name="id">mets_structure_types</xsl:attribute>
            <xsl:attribute name="name">mets_structure_types</xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M47"/>
         <svrl:active-pattern>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M48"/>
      </svrl:schematron-output>
   </xsl:template>

   <!--SCHEMATRON PATTERNS-->
<svrl:text xmlns:xs="http://www.w3.org/2001/XMLSchema"
              xmlns:schold="http://www.ascc.net/xml/schematron"
              xmlns:svrl="http://purl.oclc.org/dsdl/svrl">Schematron Validatierung der MODS AP 2.0 Metadaten </svrl:text>

   <!--PATTERN titleInfo-->


	<!--RULE -->
<xsl:template match="mods:mods" priority="1000" mode="M4">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:mods"/>
      <xsl:variable name="dmdid" select="./ancestor::mets:dmdSec/@ID"/>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='volume']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='volume']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Volume']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Volume']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='monograph']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='monograph']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='article']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='article']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Article']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Article']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='issue']">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='issue']">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'titleInfo' ist nicht vorhanden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT info-->
<xsl:if test="count(mods:titleInfo)=1 and mods:titleInfo[@type][not(@type='uniform')]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="count(mods:titleInfo)=1 and mods:titleInfo[@type][not(@type='uniform')]">
            <xsl:attribute name="role">info</xsl:attribute>
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>Warnung: Eigentlich wird erwartet, dass genau ein Element 'mods:titleInfo' ohne
				type-Attribut vorkommt. Evtl. kann es ein type-Attribut mit dem Wert 'uniform'
				aufweisen, aber hier ist ein anderer Wert im einzigen 'mods:titleInfo' Element
				enthalten.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:titleInfo[not(@type)] [2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:titleInfo[not(@type)] [2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:titleInfo': Angaben weiterer Titel
				erfordern ein type-Attribut.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M4"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M4"/>
   <xsl:template match="@*|node()" priority="-2" mode="M4">
      <xsl:apply-templates select="@*|*" mode="M4"/>
   </xsl:template>

   <!--PATTERN -->
<xsl:variable name="dmdid" select="./ancestor::mets:dmdSec/@ID"/>

	  <!--RULE -->
<xsl:template match="mods:titleInfo" priority="1000" mode="M5">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:titleInfo"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:title"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:title">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:title': Dies Kindelement von 'mods:titleInfo' muss
				vorhanden sein.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="mods:title[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title' darf nicht wiederholt werden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="count(mods:nonSort)&gt;1">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="count(mods:nonSort)&gt;1">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:nonSort' darf nicht wiederholt
				werden</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='volume']]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='volume']]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Volume']]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Volume']]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='monograph']]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='monograph']]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='article']]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='article']]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Article']]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Article']]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='issue']]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='issue']]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M5"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M5"/>
   <xsl:template match="@*|node()" priority="-2" mode="M5">
      <xsl:apply-templates select="@*|*" mode="M5"/>
   </xsl:template>

   <!--PATTERN Name-->


	<!--RULE -->
<xsl:template match="mods:name[not(parent::mods:subject)]" priority="1000" mode="M6">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name[not(parent::mods:subject)]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@type='personal' or ./@type='corporate'"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./@type='personal' or ./@type='corporate'">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:name' erfordert zwingend
				ein type-Attribut mit einem Attributwert 'personal' oder 'corporate'</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="mods:displayForm[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:displayForm[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:displayForm' darf nicht wiederholt
				werden</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M6"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M6"/>
   <xsl:template match="@*|node()" priority="-2" mode="M6">
      <xsl:apply-templates select="@*|*" mode="M6"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name[not(parent::mods:subject)]" priority="1000" mode="M7">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name[not(parent::mods:subject)]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:role"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:role">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:role', das Unterelement von 'mods:name', fehlt.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M7"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M7"/>
   <xsl:template match="@*|node()" priority="-2" mode="M7">
      <xsl:apply-templates select="@*|*" mode="M7"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name[not(parent::mods:subject)][@type='corporate']" priority="1000"
                 mode="M8">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name[not(parent::mods:subject)][@type='corporate']"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:namePart"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:namePart">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:namePart', das Kindelement von 'mods:name', ist nicht
				vorhanden. Körperschaften sollten nur mit dem Element 'mods:namePart' ausgezeichnet
				werden. Wenn stattdessen das Element 'mods:displayFrom' verwendet wurde, muss es in
				'mods:namePart' umbenannt werden.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="mods:namepart/@type">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:namepart/@type">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:namePart' darf bei Körperschaften kein Attribut
				type enthalten.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:namePart[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:namePart[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:namePart' darf bei Körperschaften nicht wiederholt
				werden</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M8"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M8"/>
   <xsl:template match="@*|node()" priority="-2" mode="M8">
      <xsl:apply-templates select="@*|*" mode="M8"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name[not(parent::mods:subject)][@type='personal']" priority="1001"
                 mode="M9">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name[not(parent::mods:subject)][@type='personal']"
                       role="info"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:namePart"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:namePart">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:namePart', das Kindelement von 'mods:name', ist nicht
				vorhanden.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="mods:namePart[not(@type)]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:namePart[not(@type)]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>Dieser Hinweis betrifft eine Abweichung von dem
				MODS AP 2.0, ohne ein Hindernis bei der Indexierung im ZVDD darzustellen: Bei einem
				Personennamen fehlt das type-Attribut in einem Kindelement 'mods:namePart'. Entweder
				muss das Attribut ergänzt oder das Element umbenannt werden. Die einzelnen Teile
				eines Personennamens werden durch den Wert des type-Attributs ('family', 'given',
				'date' oder 'termsOfAddress') näher spezifiziert. Wenn der Name vollständig in einem
				Textknoten vorkommt, muss er mit dem Element 'mods:displayForm' ausgezeichnet
				werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M9"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="mods:name[not(parent::mods:subject)][@type='personal']" priority="1000"
                 mode="M9">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name[not(parent::mods:subject)][@type='personal']"
                       role="info"/>

		    <!--REPORT -->
<xsl:if test="mods:namePart[4]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:namePart[4]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:namePart' darf nicht mehr als 3 Mal zur Angabe
				eines Personennamens wiederholt werden. Wenn ein Element den vollständigen Namen
				enthält, muss es durch das Element 'mods:displayForm' ersetzt werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M9"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M9"/>
   <xsl:template match="@*|node()" priority="-2" mode="M9">
      <xsl:apply-templates select="@*|*" mode="M9"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name[not(parent::mods:subject)][@type='personal']/mods:namePart[@type]"
                 priority="1000"
                 mode="M10">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name[not(parent::mods:subject)][@type='personal']/mods:namePart[@type]"
                       role="info"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@type='family' or ./@type='given' or ./@type='date' or ./@type='termsOfAddress'"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./@type='family' or ./@type='given' or ./@type='date' or ./@type='termsOfAddress'">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>Dieser Hinweis betrifft eine Abweichung von dem MODS AP 2.0, ohne ein Hindernis bei
				der Indexierung im ZVDD darzustellen: 'mods:namePart' hat einen ungültigen
				Attributwert.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M10"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M10"/>
   <xsl:template match="@*|node()" priority="-2" mode="M10">
      <xsl:apply-templates select="@*|*" mode="M10"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name[not(parent::mods:subject)][@type='personal']/mods:namePart"
                 priority="1000"
                 mode="M11">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name[not(parent::mods:subject)][@type='personal']/mods:namePart"
                       role="info"/>

		    <!--REPORT -->
<xsl:if test="contains(.,';')">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="contains(.,';')">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>Warnung: 'mods:namePart': Enthält das Element namePart
				Angaben zu verschiedene Personen? "<xsl:text/>
               <xsl:value-of select="."/>
               <xsl:text/>"muss </svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M11"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M11"/>
   <xsl:template match="@*|node()" priority="-2" mode="M11">
      <xsl:apply-templates select="@*|*" mode="M11"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name/mods:role" priority="1000" mode="M12">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name/mods:role"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:roleTerm"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:roleTerm">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:roleTerm', das Kindelement von 'role', ist nicht
				vorhanden</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="mods:roleTerm[3]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:roleTerm[3]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:roleTerm' darf nicht mehr als 2 Mal vorkommen (für
				die type-Attribute 'code' und 'text')</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:roleTerm[@type='code']"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:roleTerm[@type='code']">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'roleTerm' muss einen Code
				enthalten.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M12"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M12"/>
   <xsl:template match="@*|node()" priority="-2" mode="M12">
      <xsl:apply-templates select="@*|*" mode="M12"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name/mods:role/mods:roleTerm[@type='code']" priority="1000"
                 mode="M13">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name/mods:role/mods:roleTerm[@type='code']"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@authority='marcrelator' or ./@authorityURI='http://id.loc.gov/vocabulary/relators' or ./@valueURI"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./@authority='marcrelator' or ./@authorityURI='http://id.loc.gov/vocabulary/relators' or ./@valueURI">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>Die vorgeschriebene Quellenangabe für die Codes der Rolle der Person/Körperschaft
				(marcrelator) fehlt oder enthält einen ungültigen Attributwert. Erlaubt sind
				[@authority='marcrelator'], [@authorityURI='http://id.loc.gov/vocabulary/relators']
				oder eine [@valueURI]. </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M13"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M13"/>
   <xsl:template match="@*|node()" priority="-2" mode="M13">
      <xsl:apply-templates select="@*|*" mode="M13"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:name/mods:role" priority="1000" mode="M14">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:name/mods:role"/>

		    <!--ASSERT -->
<xsl:choose>
   <xsl:when test="document(concat($currentXSLFolder, 'relators.rdf'))//madsrdf:Authority[madsrdf:code = normalize-space(current()/mods:roleTerm)]"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="document(concat($currentXSLFolder, 'relators.rdf'))//madsrdf:Authority[madsrdf:code = normalize-space(current()/mods:roleTerm)]">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:roleTerm' ist ungueltig: <xsl:text/>
                  <xsl:value-of select="./mods:roleTerm"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M14"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M14"/>
   <xsl:template match="@*|node()" priority="-2" mode="M14">
      <xsl:apply-templates select="@*|*" mode="M14"/>
   </xsl:template>

   <!--PATTERN originInfo_Wurzelstrukturelement-->


	<!--RULE -->
<xsl:template match="//mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID]" priority="1000"
                 mode="M15">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="//mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID]"/>
      <xsl:variable name="div_dmdid" select="./@DMDID"/>

		    <!--REPORT -->
<xsl:if test="(./@TYPE='Volume' or ./@TYPE='volume' or ./@TYPE='Monograph' or ./@TYPE='monograph') and not(//mets:dmdSec[@ID=$div_dmdid]//mods:mods/mods:originInfo)">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="(./@TYPE='Volume' or ./@TYPE='volume' or ./@TYPE='Monograph' or ./@TYPE='monograph') and not(//mets:dmdSec[@ID=$div_dmdid]//mods:mods/mods:originInfo)">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:originInfo': Hier wurde der Strukturtyp "Monographie" oder "Band" ohne einen
				Erscheinungsvermerk in der bibliographischen Beschreibung vergeben.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M15"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M15"/>
   <xsl:template match="@*|node()" priority="-2" mode="M15">
      <xsl:apply-templates select="@*|*" mode="M15"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:mods[mods:originInfo]" priority="1000" mode="M16">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:mods[mods:originInfo]"
                       role="info"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:originInfo[2]"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:originInfo[2]">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>Dieser Hinweis betrifft eine Abweichung von dem MODS
				AP 2.0, ohne ein Hindernis bei der Indexierung im ZVDD darzustellen: Ein weiteres
				'mods:originInfo' kann dazu verwenden werden, um auf Ihre Einrichtung als
				'mods:publisher' der digitalisierten Ausgabe hinzuweisen. Dieser
				Sekundärausgabevermerk muss die Elemente 'mods:dateCaptured' und 'mods:edition' mit
				dem Inhalt [Electronic ed.] enthalten.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M16"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M16"/>
   <xsl:template match="@*|node()" priority="-2" mode="M16">
      <xsl:apply-templates select="@*|*" mode="M16"/>
   </xsl:template>

   <!--PATTERN placeTerm-->


	<!--RULE -->
<xsl:template match="mods:place/mods:placeTerm" priority="1000" mode="M17">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:place/mods:placeTerm"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="."/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test=".">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:placeTerm', Kindelement von 'originInfo/place', ist nicht
				vorhanden.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="contains(.,';')">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="contains(.,';')">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:placeTerm' enthält vermutlich mehrere Ortsnamen:
					'<xsl:text/>
               <xsl:value-of select="."/>
               <xsl:text/>'.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="contains(.,':')">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="contains(.,':')">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:placeTerm' enthält eventuell eine Verlegerangabe:
					'<xsl:text/>
               <xsl:value-of select="."/>
               <xsl:text/>'.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M17"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M17"/>
   <xsl:template match="@*|node()" priority="-2" mode="M17">
      <xsl:apply-templates select="@*|*" mode="M17"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[not(mods:edition='[Electronic ed.]')]" priority="1000"
                 mode="M18">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[not(mods:edition='[Electronic ed.]')]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:dateIssued"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:dateIssued">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo' (print) muss ein Unterelement
				mods:dateIssued enthalten.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M18"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M18"/>
   <xsl:template match="@*|node()" priority="-2" mode="M18">
      <xsl:apply-templates select="@*|*" mode="M18"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[@keyDate]]"
                 priority="1000"
                 mode="M19">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[@keyDate]]"/>

		    <!--REPORT -->
<xsl:if test="mods:dateIssued[@keyDate][2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:dateIssued[@keyDate][2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:originInfo' (print): Ein
				keyDate-Attribut darf nicht wiederholt werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M19"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M19"/>
   <xsl:template match="@*|node()" priority="-2" mode="M19">
      <xsl:apply-templates select="@*|*" mode="M19"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[not(mods:edition='[Electronic ed.]')]/mods:dateIssued[@keyDate or @point]"
                 priority="1000"
                 mode="M20">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[not(mods:edition='[Electronic ed.]')]/mods:dateIssued[@keyDate or @point]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="@encoding"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="@encoding">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo' (print): Das Format eines Schlüsseldatums und
				eines Zeitraums muss genannt werden ([@encoding='iso8601']. Zeitangaben werden nach
				ISO 8601 standardisiert bzw. nach dem älteren Standard W3CDTF.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M20"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M20"/>
   <xsl:template match="@*|node()" priority="-2" mode="M20">
      <xsl:apply-templates select="@*|*" mode="M20"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[mods:dateIssued[@point='start']]" priority="1000"
                 mode="M21">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[mods:dateIssued[@point='start']]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:dateIssued[@point='end']"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:dateIssued[@point='end']">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo' (print): Entweder muss
				das Attribut [@point] im Element 'mods:dateIssued' bei Zeiträumen paarweise mit den
				Attributwerten 'start' und 'end' vorkommen oder bei Zeitpunkten entfernt
				werden.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M21"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M21"/>
   <xsl:template match="@*|node()" priority="-2" mode="M21">
      <xsl:apply-templates select="@*|*" mode="M21"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[@point]]"
                 priority="1000"
                 mode="M22">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[@point]]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./mods:dateIssued[@keyDate][1]"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./mods:dateIssued[@keyDate][1]">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo' (print): In einem
				Zeitraum muss eine Zeitangabe als Schlüsseldatum (mods:dateIssued [@keyDate]) und
				beide Zeitangaben nach ISO 8601 bzw. nach dem älteren Standard W3CDTF
				[@encoding='iso8601'] gekennzeichnet werden. Wenn kein Schlüsseldatum bzw. kein
				standardkonformes Datum bekannt ist, schlagen wir ein künstlich generiertes keyDate
				am Anfang des Zeitraums vor. </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M22"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M22"/>
   <xsl:template match="@*|node()" priority="-2" mode="M22">
      <xsl:apply-templates select="@*|*" mode="M22"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[not(@point)]]"
                 priority="1000"
                 mode="M23">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[not(@point)]]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./mods:dateIssued[@keyDate][1]"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./mods:dateIssued[@keyDate][1]">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo' (print): Es fehlt ein
				'keyDate' mit der Codierung 'iso8601' (mods:dateIssued [@keyDate="yes"]
				[@encoding="iso8601"]). Wenn beide Attribute fehlen, weil kein standardkonformes
				Datum bekannt ist, schlagen wir ein künstlich generiertes 'keyDate' am Anfang eines
				geschätzten Zeitraums vor. Die Codierung von Zeitangaben kann auch noch nach einem
				älteren Standard W3CDTF erfolgen. </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M23"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M23"/>
   <xsl:template match="@*|node()" priority="-2" mode="M23">
      <xsl:apply-templates select="@*|*" mode="M23"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo/*[(number(normalize-space(substring(.,1,2))))&gt;0][normalize-space(substring(.,5,1))][@encoding]"
                 priority="1001"
                 mode="M24">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo/*[(number(normalize-space(substring(.,1,2))))&gt;0][normalize-space(substring(.,5,1))][@encoding]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="contains((normalize-space(substring(.,5,1))),'-')"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="contains((normalize-space(substring(.,5,1))),'-')">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo': Mit
				'iso8601' bzw. mit dem älteren Standard 'w3cdtf' codierte Erscheinungsjahre müssen
				dem Format YYYY[-MM-DD] entsprechen. Lösungsbeispiele vgl. MODS AP 2.0, S.
				21f</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M24"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="mods:originInfo/*[@encoding]" priority="1000" mode="M24">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo/*[@encoding]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="number(normalize-space(substring(.,1,4)))&gt;1"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="number(normalize-space(substring(.,1,4)))&gt;1">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo': Mit
				'iso8601' bzw. mit dem älteren Standard 'w3cdtf' codierte Erscheinungsjahre müssen
				dem Format YYYY[-MM-DD] entsprechen. Lösungsbeispiele vgl. MODS AP 2.0, S.
				21f</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M24"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M24"/>
   <xsl:template match="@*|node()" priority="-2" mode="M24">
      <xsl:apply-templates select="@*|*" mode="M24"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[mods:edition='[Electronic ed.]']/mods:dateIssued"
                 priority="1000"
                 mode="M25">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[mods:edition='[Electronic ed.]']/mods:dateIssued"/>

		    <!--REPORT -->
<xsl:if test=".">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test=".">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:originInfo' (digi): Für die Angabe des Erscheinungsdatums des
				Digitalisats muss mods:dateCaptured verwendet werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M25"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M25"/>
   <xsl:template match="@*|node()" priority="-2" mode="M25">
      <xsl:apply-templates select="@*|*" mode="M25"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:originInfo[mods:edition='[Electronic ed.]']/mods:dateCaptured"
                 priority="1000"
                 mode="M26">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:originInfo[mods:edition='[Electronic ed.]']/mods:dateCaptured"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@encoding='iso8601'"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./@encoding='iso8601'">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:originInfo' (digi): Die Angabe, wie das
				Erscheinungsdatum des Digitalisats kodiert ist, fehlt oder lautet nicht
				'iso8601'.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M26"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M26"/>
   <xsl:template match="@*|node()" priority="-2" mode="M26">
      <xsl:apply-templates select="@*|*" mode="M26"/>
   </xsl:template>

   <!--PATTERN language-->


	<!--RULE -->
<xsl:template match="mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[@TYPE='Volume' or @TYPE='volume' or @TYPE='Monograph' or @TYPE='monograph' or @TYPE='article'][@DMDID]"
                 priority="1000"
                 mode="M27">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[@TYPE='Volume' or @TYPE='volume' or @TYPE='Monograph' or @TYPE='monograph' or @TYPE='article'][@DMDID]"
                       role="info"/>
      <xsl:variable name="DMDID" select="./@DMDID"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="ancestor::mets:mets/mets:dmdSec[@ID=$DMDID]//mods:language"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="ancestor::mets:mets/mets:dmdSec[@ID=$DMDID]//mods:language">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>Dieser Hinweis
				betrifft eine Abweichung von dem MODS AP 2.0, ohne ein Hindernis bei der Indexierung
				im ZVDD darzustellen: 'mods:language' muss in Bänden und Monographien vorkommen,
				wenn es sich dabei überwiegend um Text handelt. (Das Element mods:language fehlt in
				der DMDSEC mit der ID <xsl:text/>
                  <xsl:value-of select="$DMDID"/>
                  <xsl:text/>.)</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M27"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M27"/>
   <xsl:template match="@*|node()" priority="-2" mode="M27">
      <xsl:apply-templates select="@*|*" mode="M27"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:language" priority="1000" mode="M28">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:language"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:languageTerm"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:languageTerm">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:languageTerm' ist nicht vorhanden.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:languageTerm[@type='code'][@authority='iso639-2b'][1]"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:languageTerm[@type='code'][@authority='iso639-2b'][1]">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:languageTerm': Eine Kodierung der Textsprache fehlt oder ist nicht ISO
				639-2b.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="mods:languageTerm[@type='code'][@authority='iso639-2b'][2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:languageTerm[@type='code'][@authority='iso639-2b'][2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:languageTerm': Eine Kodierung der Textsprache mit ISO 639-2b darf nicht
				wiederholt werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M28"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M28"/>
   <xsl:template match="@*|node()" priority="-2" mode="M28">
      <xsl:apply-templates select="@*|*" mode="M28"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:language/mods:languageTerm[@authority='iso639-2b']" priority="1000"
                 mode="M29">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:language/mods:languageTerm[@authority='iso639-2b']"/>

		    <!--ASSERT -->
<xsl:choose>
   <xsl:when test="document(concat($currentXSLFolder, 'iso639-2.rdf'))//madsrdf:code=normalize-space(current())"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="document(concat($currentXSLFolder, 'iso639-2.rdf'))//madsrdf:code=normalize-space(current())">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:languageTerm' hat einen falschen Sprachcode: <xsl:text/>
                  <xsl:value-of select="."/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M29"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M29"/>
   <xsl:template match="@*|node()" priority="-2" mode="M29">
      <xsl:apply-templates select="@*|*" mode="M29"/>
   </xsl:template>

   <!--PATTERN physicalDescription-->


	<!--RULE -->
<xsl:template match="mods:mods" priority="1000" mode="M30">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:mods"/>

		    <!--REPORT -->
<xsl:if test="mods:physicalDescription[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:physicalDescription[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:physicalDescription' darf nicht
				wiederholt werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M30"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M30"/>
   <xsl:template match="@*|node()" priority="-2" mode="M30">
      <xsl:apply-templates select="@*|*" mode="M30"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:physicalDescription" priority="1000" mode="M31">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:physicalDescription"/>

		    <!--REPORT -->
<xsl:if test="mods:digitalOrigin[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:digitalOrigin[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:digitalOrigin'
				darf nicht wiederholt werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M31"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M31"/>
   <xsl:template match="@*|node()" priority="-2" mode="M31">
      <xsl:apply-templates select="@*|*" mode="M31"/>
   </xsl:template>

   <!--PATTERN Anmerkung-->


	<!--RULE -->
<xsl:template match="mods:note" priority="1000" mode="M32">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:note"
                       role="info"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="@type"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="@type">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>Dieser Hinweis betrifft eine Abweichung von dem MODS AP 2.0, ohne
				ein Hindernis bei der Indexierung im ZVDD darzustellen: 'mods:note' erfordert ein
				type-Attribut mit einem Wert aus der Liste für MODS Note Types.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M32"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M32"/>
   <xsl:template match="@*|node()" priority="-2" mode="M32">
      <xsl:apply-templates select="@*|*" mode="M32"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:note[@type]" priority="1000" mode="M33">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:note[@type]"
                       role="info"/>

		    <!--ASSERT -->
<xsl:choose>
   <xsl:when test="document(concat($currentXSLFolder, 'mods_note_types.xml'))/mods_note_types[note_type = normalize-space(current()/@type)]"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="document(concat($currentXSLFolder, 'mods_note_types.xml'))/mods_note_types[note_type = normalize-space(current()/@type)]">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>Dieser Hinweis betrifft eine Abweichung von dem MODS AP 2.0, ohne ein Hindernis bei
				der Indexierung im ZVDD darzustellen: 'mods:note': '<xsl:text/>
                  <xsl:value-of select="./@type"/>
                  <xsl:text/>'
				ist kein gültiger Attributwert aus den MODS Note Types, vgl. MODS AP 2.0 bzw.
				http://www.loc.gov/standards/mods/mods-notes.html.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M33"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M33"/>
   <xsl:template match="@*|node()" priority="-2" mode="M33">
      <xsl:apply-templates select="@*|*" mode="M33"/>
   </xsl:template>

   <!--PATTERN subject-->


	<!--RULE -->
<xsl:template match="mods:subject[@valueURI or child::*[@valueURI]]" priority="1000"
                 mode="M34">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:subject[@valueURI or child::*[@valueURI]]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./mods:*[@valueURI]"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./mods:*[@valueURI]">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:subject': ein valueURI muss im Unterelement des
				mods:subject stehen.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M34"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M34"/>
   <xsl:template match="@*|node()" priority="-2" mode="M34">
      <xsl:apply-templates select="@*|*" mode="M34"/>
   </xsl:template>

   <!--PATTERN relatedItem-->


	<!--RULE -->
<xsl:template match="mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[(@TYPE='Volume') or (@TYPE='volume')][@DMDID]"
                 priority="1000"
                 mode="M35">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[(@TYPE='Volume') or (@TYPE='volume')][@DMDID]"/>
      <xsl:variable name="dmdid" select="./@DMDID"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./ancestor::mets:mets/mets:dmdSec[@ID=$dmdid]//mods:relatedItem[@type='host']"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./ancestor::mets:mets/mets:dmdSec[@ID=$dmdid]//mods:relatedItem[@type='host']">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:relatedItem[@type='host'] fehlt als Kontextknoten für die Gesamttitelangabe
				des vorliegenden Bands.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M35"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M35"/>
   <xsl:template match="@*|node()" priority="-2" mode="M35">
      <xsl:apply-templates select="@*|*" mode="M35"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:relatedItem[@type='host']" priority="1001" mode="M36">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:relatedItem[@type='host']"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:titleInfo or mods:recordInfo"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:titleInfo or mods:recordInfo">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:relatedItem[@type='host']' muss
				'mods:titleInfo' oder 'mods:recordInfo' enthalten.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./parent::mods:*/mods:part"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./parent::mods:*/mods:part">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:part' muss auf der gleichen Ebene wie
				'mods:relatedItem[@type='host']' vorkommen.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M36"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="mods:relatedItem[@type='series']" priority="1000" mode="M36">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:relatedItem[@type='series']"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:titleInfo"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:titleInfo">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:titleInfo' fehlt.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M36"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M36"/>
   <xsl:template match="@*|node()" priority="-2" mode="M36">
      <xsl:apply-templates select="@*|*" mode="M36"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:mods[mods:relatedItem[@type='host']]/mods:part" priority="1000"
                 mode="M37">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:mods[mods:relatedItem[@type='host']]/mods:part"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="@order"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="@order">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:part' muss ein order-Attribut enthalten, wenn es sich auf
				ein 'mods:relatedItem[@type='host']' bezieht.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M37"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M37"/>
   <xsl:template match="@*|node()" priority="-2" mode="M37">
      <xsl:apply-templates select="@*|*" mode="M37"/>
   </xsl:template>

   <!--PATTERN Teile-->


	<!--RULE -->
<xsl:template match="mods:part" priority="1000" mode="M38">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:part"/>

		    <!--REPORT -->
<xsl:if test="mods:detail[2][not(@type)]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:detail[2][not(@type)]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:detail'-Elemente müssen jeweils ein
				type-'Attribut aufweisen, wenn sie wiederholt werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:detail[3]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:detail[3]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:detail' darf nur einmal wiederholt werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:detail/mods:number[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:detail/mods:number[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:number' darf nicht wiederholt
				werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M38"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M38"/>
   <xsl:template match="@*|node()" priority="-2" mode="M38">
      <xsl:apply-templates select="@*|*" mode="M38"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:part[not(@type='constituent')]" priority="1000" mode="M39">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:part[not(@type='constituent')]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:detail/mods:number"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:detail/mods:number">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:detail/mods:number' ist in 'mods:part'
				nicht vorhanden</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M39"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M39"/>
   <xsl:template match="@*|node()" priority="-2" mode="M39">
      <xsl:apply-templates select="@*|*" mode="M39"/>
   </xsl:template>

   <!--PATTERN Wurzelstrukturelement_recordInfo-->


	<!--RULE -->
<xsl:template match="//mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID]"
                 priority="1000"
                 mode="M40">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="//mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID]"/>
      <xsl:variable name="div_dmdid" select="./@DMDID"/>

		    <!--REPORT -->
<xsl:if test="(./@TYPE='Volume' or ./@TYPE='volume' or ./@TYPE='Monograph' or ./@TYPE='monograph') and not(//mets:dmdSec[@ID=$div_dmdid]//mods:mods/mods:recordInfo)">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="(./@TYPE='Volume' or ./@TYPE='volume' or ./@TYPE='Monograph' or ./@TYPE='monograph') and not(//mets:dmdSec[@ID=$div_dmdid]//mods:mods/mods:recordInfo)">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:recordInfo' muss in der bibliographischen Beschreibung von Monographien und
				Bänden enthalten sein (z. B. zum Referenzieren).</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M40"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M40"/>
   <xsl:template match="@*|node()" priority="-2" mode="M40">
      <xsl:apply-templates select="@*|*" mode="M40"/>
   </xsl:template>

   <!--PATTERN SatzID-->


	<!--RULE -->
<xsl:template match="mods:recordInfo" priority="1001" mode="M41">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:recordInfo"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:recordIdentifier"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:recordIdentifier">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>mods:recordIdentifier fehlt</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--REPORT -->
<xsl:if test="mods:recordIdentifier[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:recordIdentifier[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>mods:recordIdentifier darf nicht wiederholt
				werden</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M41"/>
   </xsl:template>

	  <!--RULE -->
<xsl:template match="mods:recordInfo/mods:recordIdentifier" priority="1000" mode="M41">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:recordInfo/mods:recordIdentifier"/>

		    <!--REPORT -->
<xsl:if test="contains(.,' ')">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="contains(.,' ')">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>mods:recordIdentifier enthaelt Leerzeichen</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M41"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M41"/>
   <xsl:template match="@*|node()" priority="-2" mode="M41">
      <xsl:apply-templates select="@*|*" mode="M41"/>
   </xsl:template>

   <!--PATTERN identifier-->


	<!--RULE -->
<xsl:template match="mods:identifier" priority="1000" mode="M42">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:identifier"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="./@type"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="./@type">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:identifier' erfordert zwingend ein type-Attribut</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M42"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M42"/>
   <xsl:template match="@*|node()" priority="-2" mode="M42">
      <xsl:apply-templates select="@*|*" mode="M42"/>
   </xsl:template>

   <!--PATTERN location-->


	<!--RULE -->
<xsl:template match="mods:location[not(mods:shelfLocator)]" priority="1000" mode="M43">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:location[not(mods:shelfLocator)]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:physicalLocation or mods:url"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:physicalLocation or mods:url">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text>'mods:location' muss wenigstens
				entweder 'mods:physicalLocation' oder 'mods:url' aufweisen.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M43"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M43"/>
   <xsl:template match="@*|node()" priority="-2" mode="M43">
      <xsl:apply-templates select="@*|*" mode="M43"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:location" priority="1000" mode="M44">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:location"/>

		    <!--REPORT -->
<xsl:if test="mods:physicalLocation[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:physicalLocation[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:physicalLocation' darf nicht wiederholt
				werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>

		    <!--REPORT -->
<xsl:if test="mods:shelfLocator[2]">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="mods:shelfLocator[2]">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'mods:shelfLocator' darf nicht wiederholt
				werden.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M44"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M44"/>
   <xsl:template match="@*|node()" priority="-2" mode="M44">
      <xsl:apply-templates select="@*|*" mode="M44"/>
   </xsl:template>

   <!--PATTERN -->


	<!--RULE -->
<xsl:template match="mods:location[mods:shelfLocator]" priority="1000" mode="M45">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="mods:location[mods:shelfLocator]"/>

		    <!--ASSERT -->
<xsl:choose>
         <xsl:when test="mods:physicalLocation"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:schold="http://www.ascc.net/xml/schematron"
                                xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="mods:physicalLocation">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-get-full-path"/>
               </xsl:attribute>
               <svrl:text> Der Besitzer des Original (Drucks)
				'mods:physicalLocation' und eine Signatur 'mods:shelfLocator' müssen zusammen in
				'mods:location' vorkommen.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*|*" mode="M45"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M45"/>
   <xsl:template match="@*|node()" priority="-2" mode="M45">
      <xsl:apply-templates select="@*|*" mode="M45"/>
   </xsl:template>

   <!--PATTERN emptyElements-->


	<!--RULE -->
<xsl:template match="//mods:*" priority="1000" mode="M46">
      <svrl:fired-rule xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:schold="http://www.ascc.net/xml/schematron"
                       xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="//mods:*"/>

		    <!--REPORT -->
<xsl:if test="not(./child::*) and normalize-space(.)=''">
         <svrl:successful-report xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                 xmlns:schold="http://www.ascc.net/xml/schematron"
                                 xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                 test="not(./child::*) and normalize-space(.)=''">
            <xsl:attribute name="location">
               <xsl:apply-templates select="." mode="schematron-get-full-path"/>
            </xsl:attribute>
            <svrl:text>'<xsl:text/>
               <xsl:value-of select="name(.)"/>
               <xsl:text/>' hat keinen Inhalt.
				Jedes Element muss ein Textelement oder ein Kindelement mit einem Textelement
				enthalten.</svrl:text>
         </svrl:successful-report>
      </xsl:if>
      <xsl:apply-templates select="@*|*" mode="M46"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M46"/>
   <xsl:template match="@*|node()" priority="-2" mode="M46">
      <xsl:apply-templates select="@*|*" mode="M46"/>
   </xsl:template>

   <!--PATTERN mets_structure_types-->
<xsl:template match="text()" priority="-1" mode="M47"/>
   <xsl:template match="@*|node()" priority="-2" mode="M47">
      <xsl:apply-templates select="@*|*" mode="M47"/>
   </xsl:template>

   <!--PATTERN -->
<xsl:template match="text()" priority="-1" mode="M48"/>
   <xsl:template match="@*|node()" priority="-2" mode="M48">
      <xsl:apply-templates select="@*|*" mode="M48"/>
   </xsl:template>
</xsl:stylesheet>