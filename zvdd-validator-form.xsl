<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:php="http://php.net/xsl"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

	<xsl:output method="html" indent="yes" omit-xml-declaration="yes"/>

	<xsl:param name="argument-formParameters-type"/>
	<xsl:param name="argument-formParameters-url"/>
	<xsl:param name="argument-formParameters-oai"/>
	<xsl:param name="argument-formParameters-xml"/>
	<xsl:param name="argument-formParameters-file"/>
	<xsl:param name="argument-formParameters-file-name"/>
	<xsl:param name="argument-formParameters-file-size"/>


	<xsl:template match="/">
		<html:div class="zvdd-validator">
			<xsl:variable name="type">
				<xsl:choose>
					<xsl:when test="$argument-formParameters-type = 'xml' or $argument-formParameters-type = 'url' or $argument-formParameters-type = 'oai' or $argument-formParameters-type = 'file'">
						<xsl:value-of select="$argument-formParameters-type"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>url</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<form class="rewrite-off" method="GET" enctype="multipart/form-data" onsubmit="return submitForm();">
				<ul class="validate-menu">
					<li>
						<xsl:if test="$type = 'url'">
							<xsl:attribute name="class">current</xsl:attribute>
						</xsl:if>
						<input type="radio" name="type" value="url" id="type-url" onclick="selectItem(event)">
							<xsl:if test="$type = 'url'">
								<xsl:attribute name="checked"/>
							</xsl:if>
						</input>
						<label for="type-url">von URL laden</label>
					</li>
					<li>
						<xsl:if test="$type = 'oai'">
							<xsl:attribute name="class">current</xsl:attribute>
						</xsl:if>
						<input type="radio" name="type" value="oai" id="type-oai" onclick="selectItem(event)">
							<xsl:if test="$type = 'oai'">
								<xsl:attribute name="checked"/>
							</xsl:if>
						</input>
						<label for="type-oai">von OAI laden</label>
					</li>
					<li>
						<xsl:if test="$type = 'xml'">
							<xsl:attribute name="class">current</xsl:attribute>
						</xsl:if>
						<input type="radio" name="type" value="xml" id="type-xml" onclick="selectItem(event)">
							<xsl:if test="$type = 'xml'">
								<xsl:attribute name="checked"/>
							</xsl:if>
						</input>
						<label for="type-xml">XML eingeben</label>
					</li>
					<li>
						<xsl:if test="$type = 'file'">
							<xsl:attribute name="class">current</xsl:attribute>
						</xsl:if>
						<input type="radio" name="type" value="file" id="type-file" onclick="selectItem(event)">
							<xsl:if test="$type = 'file'">
								<xsl:attribute name="checked"/>
							</xsl:if>
						</input>
						<label for="type-file">Datei hochladen</label>
					</li>
				</ul>

				<p>
					<div>
						<xsl:attribute name="class">
							<xsl:text>form-item</xsl:text>
							<xsl:if test="$type = 'url'"> current</xsl:if>
						</xsl:attribute>
						<label for="input-url">URL:</label>
						<input id="input-url" name="url" type="text" value="{$argument-formParameters-url}" placeholder="z.B. http://www.dilibri.de/oai/?verb=GetRecord&amp;metadataPrefix=mets&amp;identifier=206800"/>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>form-item</xsl:text>
							<xsl:if test="$type = 'oai'"> current</xsl:if>
						</xsl:attribute>
						<label for="input-oai">OAI URL:</label>
						<input id="input-oai" name="oai" type="text" value="{$argument-formParameters-oai}" placeholder="z.B. http://digi.bib.uni-mannheim.de/oai"/>
						<p>
							Den ersten Download Daten mit metadataPrefix=mets überprüfen.
						</p>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>form-item</xsl:text>
							<xsl:if test="$type = 'xml'"> current</xsl:if>
						</xsl:attribute>
						<label for="input-xml">XML:</label>
						<textarea id="input-xml" name="xml" onchange="textChange()" onkeyup="textChange()">
							<xsl:value-of select="$argument-formParameters-xml"/>
						</textarea>
						<div class="validationError hidden">XML Fehler</div>
					</div>
					<div>
						<xsl:attribute name="class">
							<xsl:text>form-item</xsl:text>
							<xsl:if test="$type = 'file'"> current</xsl:if>
						</xsl:attribute>
						<label for="input-file">Datei:</label>
						<input id="input-file" name="file" type="file"/>
					</div>
					<div class="form-item2">
						<input class="send-button" name="Abschicken" type="submit"/>
					</div>
				</p>
			</form>
			
			<xsl:if test="/html:html/html:body">
				<h4>
					<xsl:text>Ergebnisse</xsl:text>
					<xsl:choose>
						<xsl:when test="$argument-formParameters-type = 'url'">
							<xsl:text> für URL »</xsl:text>
							<a class="rewrite-off">
								<xsl:attribute name="href">
									<xsl:value-of select="$argument-formParameters-url"/>
								</xsl:attribute>
								<xsl:value-of select="$argument-formParameters-url"/>
							</a>
							<xsl:text>«</xsl:text>
						</xsl:when>
						<xsl:when test="$argument-formParameters-type = 'oai'">
							<xsl:text> für OAI URL »</xsl:text>
							<a>
								<xsl:variable name="oai-url">
									<xsl:value-of select="$argument-formParameters-oai"/>
									<xsl:text>?verb=ListRecords&amp;metadataPrefix=mets</xsl:text>
								</xsl:variable>
								<xsl:attribute name="href">
									<xsl:value-of select="$oai-url"/>
								</xsl:attribute>
								<xsl:value-of select="$oai-url"/>
							</a>
							<xsl:text>«</xsl:text>
						</xsl:when>
						<xsl:when test="$argument-formParameters-type = 'xml'">
							<xsl:text> für eingegebenes XML (</xsl:text>
							<xsl:value-of select="string-length($argument-formParameters-xml)"/>
							<xsl:text> Zeichen)</xsl:text>
						</xsl:when>
						<xsl:when test="$argument-formParameters-type = 'file'">
							<xsl:text>  für hochgeladene Datei »</xsl:text>
							<xsl:value-of select="$argument-formParameters-file-name"/>
							<xsl:text>« (</xsl:text>
							<xsl:value-of select="$argument-formParameters-file-size"/>
							<xsl:text> Zeichen)</xsl:text>
						</xsl:when>
					</xsl:choose>
				</h4>

				<xsl:copy-of select="/html:html/html:body/*"/>
			</xsl:if>

		</html:div>
	</xsl:template>
	
</xsl:stylesheet>