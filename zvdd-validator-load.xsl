<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:php="http://php.net/xsl"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

	<xsl:output method="xml" indent="yes"/>

	<xsl:param name="argument-formParameters-type"/>
	<xsl:param name="argument-formParameters-url"/>
	<xsl:param name="argument-formParameters-xml"/>
	<xsl:param name="argument-formParameters-oai"/>
	<xsl:param name="argument-formParameters-file"/>
	<xsl:param name="argument-formParameters-file-error"/>
	<xsl:param name="argument-formParameters-file-tmp_name"/>


	<xsl:template match="/">
			<xsl:choose>
				<xsl:when test="$argument-formParameters-type = 'xml'">
					<xsl:copy-of select="php:function('Tx_XMLInclude_Controller_XMLIncludeController::parseXML', $argument-formParameters-xml)"/>
				</xsl:when>
				<xsl:when test="$argument-formParameters-type = 'file'
								and string-length($argument-formParameters-file-tmp_name) &gt; 0
								and $argument-formParameters-file-error = '0'">
					<xsl:variable name="fileURL" select="concat('file://', $argument-formParameters-file-tmp_name)"/>
					<xsl:copy-of select="document($fileURL)"/>
				</xsl:when>
				<xsl:when test="$argument-formParameters-type = 'oai'">
					<xsl:variable name="oai-url">
						<xsl:value-of select="$argument-formParameters-oai"/>
						<xsl:text>?verb=ListRecords&amp;metadataPrefix=mets</xsl:text>
					</xsl:variable>
					<xsl:copy-of select="document($oai-url)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="document($argument-formParameters-url)"/>
				</xsl:otherwise>
			</xsl:choose>
    </xsl:template>

</xsl:stylesheet>