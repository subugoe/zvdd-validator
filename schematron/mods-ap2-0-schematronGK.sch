<schema xmlns="http://purl.oclc.org/dsdl/schematron">
	
	<title>Schematron Validatierung der MODS AP 2.0 Metadaten </title>

	<ns prefix="mets" uri="http://www.loc.gov/METS/"/>
	<ns prefix="mods" uri="http://www.loc.gov/mods/v3"/>
	<ns uri="http://www.loc.gov/mads/rdf/v1#" prefix="madsrdf"/>
	<!-- <ns prefix="fn" uri="http://www.w3.org/2005/xpath-functions"/>-->

	<!-- Validation rules -->

	<!-- Titel -->
	<pattern id="titleInfo">
		<!-- 1.1 titleInfo -->
		<rule context="mods:mods">
			<let name="dmdid" value="./ancestor::mets:dmdSec/@ID"/>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']"
				>'titleInfo' ist nicht vorhanden</report>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='volume']"
				>'titleInfo' ist nicht vorhanden</report>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Volume']"
				>'titleInfo' ist nicht vorhanden</report>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='monograph']"
				>'titleInfo' ist nicht vorhanden</report>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']"
				>'titleInfo' ist nicht vorhanden</report>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='article']"
				>'titleInfo' ist nicht vorhanden</report>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Article']"
				>'titleInfo' ist nicht vorhanden</report>
			<report
				test="./ancestor-or-self::mods:mods[not(mods:titleInfo) and not(mods:part) and not(mods:relatedItem[@type='host'])] and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='issue']"
				>'titleInfo' ist nicht vorhanden</report>
			<report role="info"
				test="count(mods:titleInfo)=1 and mods:titleInfo[@type][not(@type='uniform')]"
				>Warnung: Eigentlich wird erwartet, dass genau ein Element 'mods:titleInfo' ohne
				type-Attribut vorkommt. Evtl. kann es ein type-Attribut mit dem Wert 'uniform'
				aufweisen, aber hier ist ein anderer Wert im einzigen 'mods:titleInfo' Element
				enthalten.</report>
			<report test="mods:titleInfo[not(@type)] [2]">'mods:titleInfo': Angaben weiterer Titel
				erfordern ein type-Attribut.</report>
		</rule>
	</pattern>
	<pattern>
		<!-- 1.2.1 title & 1.2.2 nonSort -->
		<let name="dmdid" value="./ancestor::mets:dmdSec/@ID"/>
		<rule context="mods:titleInfo">
			<assert test="mods:title">'mods:title': Dies Kindelement von 'mods:titleInfo' muss
				vorhanden sein.</assert>
			<report test="mods:title[2]">'mods:title' darf nicht wiederholt werden</report>
			<report test="count(mods:nonSort)&gt;1">'mods:nonSort' darf nicht wiederholt
				werden</report>
			<report
				test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='volume']]"
				>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</report>
			<report
				test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Volume']]"
				>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</report>
			<report
				test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='monograph']]"
				>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</report>
			<report
				test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Monograph']]"
				>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</report>
			<report
				test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='article']]"
				>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</report>
			<report
				test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='Article']]"
				>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</report>
			<report
				test="mods:title[string-length(.)&lt;3 and //mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID=$dmdid][@TYPE='issue']]"
				>'mods:title': Dieser Titel besteht nur aus einer Art Bandbezeichnung. Auch Bände
				müssen einen Hauptsachtitel haben - nur Kapitel nicht. Hat die Vorlage keinen Titel,
				muss diese Element fingiert oder ggf. aus dem Gesamttitel generiert werden.</report>

		</rule>
	</pattern>

	<!-- Name -->
	<pattern id="Name">
		<!-- 2.1 name -->
		<rule context="mods:name[not(parent::mods:subject)]">
			<assert test="./@type='personal' or ./@type='corporate'">'mods:name' erfordert zwingend
				ein type-Attribut mit einem Attributwert 'personal' oder 'corporate'</assert>
			<!-- 2.2.1 namePart, 2.2.2 dislayForm, 2.2.3 role -->
			<report test="mods:displayForm[2]">'mods:displayForm' darf nicht wiederholt
				werden</report>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:name[not(parent::mods:subject)]">
			<assert test="mods:role">'mods:role', das Unterelement von 'mods:name', fehlt.</assert>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:name[not(parent::mods:subject)][@type='corporate']">
			<assert test="mods:namePart">'mods:namePart', das Kindelement von 'mods:name', ist nicht
				vorhanden. Körperschaften sollten nur mit dem Element 'mods:namePart' ausgezeichnet
				werden. Wenn stattdessen das Element 'mods:displayFrom' verwendet wurde, muss es in
				'mods:namePart' umbenannt werden.</assert>
			<report test="mods:namepart/@type">'mods:namePart' darf bei Körperschaften kein Attribut
				type enthalten.</report>
			<report test="mods:namePart[2]">'mods:namePart' darf bei Körperschaften nicht wiederholt
				werden</report>
		</rule>
	</pattern>
	<pattern>
		<rule role="info" context="mods:name[not(parent::mods:subject)][@type='personal']">
			<assert test="mods:namePart">'mods:namePart', das Kindelement von 'mods:name', ist nicht
				vorhanden.</assert>
			<report test="mods:namePart[not(@type)]">Dieser Hinweis betrifft eine Abweichung von dem
				MODS AP 2.0, ohne ein Hindernis bei der Indexierung im ZVDD darzustellen: Bei einem
				Personennamen fehlt das type-Attribut in einem Kindelement 'mods:namePart'. Entweder
				muss das Attribut ergänzt oder das Element umbenannt werden. Die einzelnen Teile
				eines Personennamens werden durch den Wert des type-Attributs ('family', 'given',
				'date' oder 'termsOfAddress') näher spezifiziert. Wenn der Name vollständig in einem
				Textknoten vorkommt, muss er mit dem Element 'mods:displayForm' ausgezeichnet
				werden.</report>
		</rule>
		<rule role="info" context="mods:name[not(parent::mods:subject)][@type='personal']">
			<report test="mods:namePart[4]">'mods:namePart' darf nicht mehr als 3 Mal zur Angabe
				eines Personennamens wiederholt werden. Wenn ein Element den vollständigen Namen
				enthält, muss es durch das Element 'mods:displayForm' ersetzt werden.</report>
		</rule>
	</pattern>
	<pattern>
		<rule role="info"
			context="mods:name[not(parent::mods:subject)][@type='personal']/mods:namePart[@type]">
			<assert
				test="./@type='family' or ./@type='given' or ./@type='date' or ./@type='termsOfAddress'"
				>Dieser Hinweis betrifft eine Abweichung von dem MODS AP 2.0, ohne ein Hindernis bei
				der Indexierung im ZVDD darzustellen: 'mods:namePart' hat einen ungültigen
				Attributwert.</assert>

		</rule>
	</pattern>

	<pattern>
		<rule role="info"
			context="mods:name[not(parent::mods:subject)][@type='personal']/mods:namePart">
			<!-- <assert
				test="./@type='family' or ./@type='given' or ./@type='date' or ./@type='termsOfAddress'"
				>Dieser Hinweis betrifft eine Abweichung von dem MODS AP 2.0, ohne ein Hindernis bei
				der Indexierung im ZVDD darzustellen: 'mods:namePart' hat einen ungültigen
				Attributwert</assert>
			<assert
				test="./@type='family' or ./@type='given' or ./@type='date' or ./@type='termsOfAddress'"
				>Warnung: 'mods:namePart': Attribut type von 'namePart' fehlt oder hat einen
				ungültigen Attributwert</assert> -->
			<report test="contains(.,';')">Warnung: 'mods:namePart': Enthält das Element namePart
				Angaben zu verschiedene Personen? "<value-of select="."/>"muss </report>
		</rule>
	</pattern>
	<pattern>
		<!-- 2.2.4 roleTerm -->
		<rule context="mods:name/mods:role">
			<assert test="mods:roleTerm">'mods:roleTerm', das Kindelement von 'role', ist nicht
				vorhanden</assert>
			<report test="mods:roleTerm[3]">'mods:roleTerm' darf nicht mehr als 2 Mal vorkommen (für
				die type-Attribute 'code' und 'text')</report>
			<assert test="mods:roleTerm[@type='code']">'roleTerm' muss einen Code
				enthalten.</assert>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:name/mods:role/mods:roleTerm[@type='code']">
			<assert
				test="./@authority='marcrelator' or ./@authorityURI='http://id.loc.gov/vocabulary/relators' or ./@valueURI"
				>Die vorgeschriebene Quellenangabe für die Codes der Rolle der Person/Körperschaft
				(marcrelator) fehlt oder enthält einen ungültigen Attributwert. Erlaubt sind
				[@authority='marcrelator'], [@authorityURI='http://id.loc.gov/vocabulary/relators']
				oder eine [@valueURI]. </assert>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:name/mods:role">
			<assert
				test="document('relators.rdf')//madsrdf:Authority[madsrdf:code = normalize-space(current()/mods:roleTerm)]"
				>'mods:roleTerm' ist ungueltig: <value-of select="./mods:roleTerm"/></assert>
		</rule>
	</pattern>
	<!-- 4.1 originInfo (Teil 1) -->
	<pattern id="originInfo_Wurzelstrukturelement">
		<rule context="//mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID]">
			<let name="div_dmdid" value="./@DMDID"/>
			<report subject="./ancestor::*/mets:dmdSec[@ID=$div_dmdid]//mods:mods"
				test="(./@TYPE='Volume' or ./@TYPE='volume' or ./@TYPE='Monograph' or ./@TYPE='monograph') and not(//mets:dmdSec[@ID=$div_dmdid]//mods:mods/mods:originInfo)"
				>'mods:originInfo': Hier wurde der Strukturtyp "Monographie" oder "Band" ohne einen
				Erscheinungsvermerk in der bibliographischen Beschreibung vergeben.</report>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:mods[mods:originInfo]" role="info">
			<assert test="mods:originInfo[2]">Dieser Hinweis betrifft eine Abweichung von dem MODS
				AP 2.0, ohne ein Hindernis bei der Indexierung im ZVDD darzustellen: Ein weiteres
				'mods:originInfo' kann dazu verwenden werden, um auf Ihre Einrichtung als
				'mods:publisher' der digitalisierten Ausgabe hinzuweisen. Dieser
				Sekundärausgabevermerk muss die Elemente 'mods:dateCaptured' und 'mods:edition' mit
				dem Inhalt [Electronic ed.] enthalten.</assert>
		</rule>
	</pattern>
	<!-- 4.2.2 placeTerm -->
	<pattern id="placeTerm">
		<rule context="mods:place/mods:placeTerm">
			<assert test=".">'mods:placeTerm', Kindelement von 'originInfo/place', ist nicht
				vorhanden.</assert>
			<report test="contains(.,';')">'mods:placeTerm' enthält vermutlich mehrere Ortsnamen:
					'<value-of select="."/>'.</report>
			<report test="contains(.,':')">'mods:placeTerm' enthält eventuell eine Verlegerangabe:
					'<value-of select="."/>'.</report>
		</rule>
	</pattern>
	<!-- 4.2.4 dateIssued -->

	<pattern>
		<rule context="mods:originInfo[not(mods:edition='[Electronic ed.]')]">
			<!-- originInfo muss im Kontextknoten stehen, weil es ein Wurzelstrukturelement ist und nur einmal im mets-Satz erscheint,
				sonst wird es inkorrekterweise in jeder mods:mods abgefragt. -->
			<assert test="mods:dateIssued">'mods:originInfo' (print) muss ein Unterelement
				mods:dateIssued enthalten.</assert>
		</rule>
	</pattern>
	<pattern>
		<rule
			context="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[@keyDate]]">
			<report test="mods:dateIssued[@keyDate][2]">'mods:originInfo' (print): Ein
				keyDate-Attribut darf nicht wiederholt werden.</report>
		</rule>
	</pattern>
	<pattern>
		<rule
			context="mods:originInfo[not(mods:edition='[Electronic ed.]')]/mods:dateIssued[@keyDate or @point]">
			<assert test="@encoding">'mods:originInfo' (print): Das Format eines Schlüsseldatums und
				eines Zeitraums muss genannt werden ([@encoding='iso8601']. Zeitangaben werden nach
				ISO 8601 standardisiert bzw. nach dem älteren Standard W3CDTF.</assert>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:originInfo[mods:dateIssued[@point='start']]">
			<assert test="mods:dateIssued[@point='end']">'mods:originInfo' (print): Entweder muss das Attribut
				[@point] im Element 'mods:dateIssued' bei Zeiträumen paarweise mit den
				Attributwerten 'start' und 'end' vorkommen oder bei Zeitpunkten entfernt
				werden.</assert>
		</rule>
	</pattern>
	<pattern>
		<rule
			context="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[@point]]">
			<assert test="./mods:dateIssued[@keyDate][1]">'mods:originInfo' (print): In einem
				Zeitraum muss eine Zeitangabe als Schlüsseldatum (mods:dateIssued [@keyDate]) und
				beide Zeitangaben nach ISO 8601 bzw. nach dem älteren Standard W3CDTF
				[@encoding='iso8601'] gekennzeichnet werden. Wenn kein Schlüsseldatum bzw. kein
				standardkonformes Datum bekannt ist, schlagen wir ein künstlich generiertes keyDate
				am Anfang des Zeitraums vor. </assert>
		</rule>
	</pattern>
	<pattern>
		<rule
			context="mods:originInfo[not(mods:edition='[Electronic ed.]')][mods:dateIssued[not(@point)]]">
			<assert test="./mods:dateIssued[@keyDate][1]">'mods:originInfo' (print): Es fehlt ein
				'keyDate' mit der Codierung 'iso8601' (mods:dateIssued [@keyDate="yes"]
				[@encoding="iso8601"]). Wenn beide Attribute fehlen, weil kein standardkonformes
				Datum bekannt ist, schlagen wir ein künstlich generiertes 'keyDate' am Anfang eines
				geschätzten Zeitraums vor. Die Codierung von Zeitangaben kann auch noch nach einem
				älteren Standard W3CDTF erfolgen. </assert>
		</rule>
	</pattern>
	<pattern>
		<!-- Datum geht nach 4 Ziffern falsch weiter. Pattern-Prinzip(1): Zuerst alles prüfen, was 
			mit Ziffern anfängt aber ab der fünften Stelle falsch ist -->
		<rule
			context="mods:originInfo/*[(number(normalize-space(substring(.,1,2))))&gt;0][normalize-space(substring(.,5,1))][@encoding]">
			<assert test="contains((normalize-space(substring(.,5,1))),'-')">'mods:originInfo': Mit
				'iso8601' bzw. mit dem älteren Standard 'w3cdtf' codierte Erscheinungsjahre müssen
				dem Format YYYY[-MM-DD] entsprechen. Lösungsbeispiele vgl. MODS AP 2.0, S.
				21f</assert>
		</rule>
		<!-- Datum fängt nicht mit Ziffer an. Pattern-Prinzip(2): Dann vom Rest finden, was nicht mit
			Ziffern beginnt. -->
		<rule context="mods:originInfo/*[@encoding]">
			<assert test="number(normalize-space(substring(.,1,4)))&gt;1">'mods:originInfo': Mit
				'iso8601' bzw. mit dem älteren Standard 'w3cdtf' codierte Erscheinungsjahre müssen
				dem Format YYYY[-MM-DD] entsprechen. Lösungsbeispiele vgl. MODS AP 2.0, S.
				21f</assert>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:originInfo[mods:edition='[Electronic ed.]']/mods:dateIssued">
			<report test=".">'mods:originInfo' (digi): Für die Angabe des Erscheinungsdatums des
				Digitalisats muss mods:dateCaptured verwendet werden.</report>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:originInfo[mods:edition='[Electronic ed.]']/mods:dateCaptured">
			<assert test="./@encoding='iso8601'">'mods:originInfo' (digi): Die Angabe, wie das
				Erscheinungsdatum des Digitalisats kodiert ist, fehlt oder lautet nicht
				'iso8601'.</assert>
		</rule>
	</pattern>
	<!-- 5.1 language -->

	<pattern id="language">
		<rule role="info"
			context="mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[@TYPE='Volume' or @TYPE='volume' or @TYPE='Monograph' or @TYPE='monograph' or @TYPE='article'][@DMDID]">
			<let name="DMDID" value="./@DMDID"/>
			<assert subject="./ancestor::mets:mets/mets:dmdSec[@ID=$DMDID]//mods:mods"
				test="./ancestor::mets:mets/mets:dmdSec[@ID=$DMDID]//mods:language">Dieser Hinweis
				betrifft eine Abweichung von dem MODS AP 2.0, ohne ein Hindernis bei der Indexierung
				im ZVDD darzustellen: 'mods:language' muss in Bänden und Monographien vorkommen,
				wenn es sich dabei überwiegend um Text handelt.</assert>
		</rule>
	</pattern>
	<!-- 5.2.1 languageTerm -->
	<pattern>
		<rule context="mods:language">
			<assert test="mods:languageTerm">'mods:languageTerm' ist nicht vorhanden.</assert>
			<assert test="mods:languageTerm[@type='code'][@authority='iso639-2b'][1]"
				>'mods:languageTerm': Eine Kodierung der Textsprache fehlt oder ist nicht ISO
				639-2b.</assert>
			<report subject="mods:languageTerm[@type='code'][@authority='iso639-2b'][2]"
				test="mods:languageTerm[@type='code'][@authority='iso639-2b'][2]"
				>'mods:languageTerm': Eine Kodierung der Textsprache mit ISO 639-2b darf nicht
				wiederholt werden.</report>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:language/mods:languageTerm[@authority='iso639-2b']">
			<assert test="document('iso639-2.rdf')//madsrdf:code=normalize-space(current())"
				>'mods:languageTerm' hat einen falschen Sprachcode: <value-of select="."/></assert>
		</rule>
	</pattern>
	<!-- 6.1 physicalDescription -->
	<pattern id="physicalDescription">
		<rule context="mods:mods">
			<report test="mods:physicalDescription[2]">'mods:physicalDescription' darf nicht
				wiederholt werden.</report>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:physicalDescription">
			<report subject="./mods:digitalOrigin" test="mods:digitalOrigin[2]">'mods:digitalOrigin'
				darf nicht wiederholt werden.</report>
		</rule>
	</pattern>
	<!-- 8.1 note -->
	<pattern id="Anmerkung">
		<rule context="mods:note" role="info">
			<assert test="./@type">Dieser Hinweis betrifft eine Abweichung von dem MODS AP 2.0, ohne
				ein Hindernis bei der Indexierung im ZVDD darzustellen: 'mods:note' erfordert ein
				type-Attribut mit einem Wert aus der Liste für MODS Note Types.</assert>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:note[@type]" role="info">
			<assert
				test="document('mods_note_types.xml')//*[note_type = normalize-space(current()/@type)]"
				>Dieser Hinweis betrifft eine Abweichung von dem MODS AP 2.0, ohne ein Hindernis bei
				der Indexierung im ZVDD darzustellen: 'mods:note': '<value-of select="./@type"/>'
				ist kein gültiger Attributwert aus den MODS Note Types.</assert>
		</rule>
	</pattern>

	<!-- 9.1 subject-->
	<pattern id="subject">
		<!-- 9.1 subject -->
		<rule context="mods:subject[@valueURI or child::*[@valueURI]]">
			<assert test="./mods:*[@valueURI]">'mods:subject': ein valueURI muss im Unterelement des
				mods:subject stehen.</assert>
		</rule>
	</pattern>
	<!-- relatedItem -->



	<pattern id="relatedItem">
		<!-- 11.1 relatedItem -->
		<rule
			context="mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[(@TYPE='Volume') or (@TYPE='volume')][@DMDID]">
			<let name="dmdid" value="./@DMDID"/>
			<assert subject="./ancestor::mets:mets/mets:dmdSec[@ID=$dmdid]//mods:mods"
				test="./ancestor::mets:mets/mets:dmdSec[@ID=$dmdid]//mods:relatedItem[@type='host']"
				>'mods:relatedItem[@type='host'] fehlt als Kontextknoten für die Gesamttitelangabe
				des vorliegenden Bands.</assert>
		</rule>
	</pattern>

	<pattern>
		<!-- 11.2.1 relatedItem-titelInfo -->
		<rule context="mods:relatedItem[@type='host']">
			<assert test="mods:titleInfo or mods:recordInfo">'mods:relatedItem[@type='host']' muss
				'mods:titleInfo' oder 'mods:recordInfo' enthalten.</assert>
			<!-- 11.2.2 & 14.1 part -->
			<assert test="./parent::mods:*/mods:part">'mods:part' muss auf der gleichen Ebene wie
				'mods:relatedItem[@type='host']' vorkommen.</assert>
		</rule>
		<rule context="mods:relatedItem[@type='series']">
			<assert test="mods:titleInfo">'mods:titleInfo' fehlt.</assert>
		</rule>
	</pattern>

	<pattern>
		<rule context="mods:mods[mods:relatedItem[@type='host']]/mods:part">
			<!-- 11.2.2 & 14.1 part -->
			<assert test="@order">'mods:part' muss ein order-Attribut enthalten, wenn es sich auf
				ein 'mods:relatedItem[@type='host']' bezieht.</assert>
		</rule>
	</pattern>

	<!-- X.1 Leere Elemente -->
	<!-- <pattern id="order_emptyAttribute">
		<rule context="mods:part/@order">
			<report test="normalize-space(.)=''">mods:part/@order hat keinen Inhalt.</report>
		</rule>
	</pattern> -->
	<!-- Teile -->
	<!-- vgl. ganz oben Test, ob mods:part irgendwo wiederholt wird. -->
	<!-- 11.3.1 detail -->
	<pattern id="Teile">
		<rule context="mods:part">
			<report test="mods:detail[2][not(@type)]">'mods:detail'-Elemente müssen jeweils ein
				type-'Attribut aufweisen, wenn sie wiederholt werden.</report>
			<report test="mods:detail[3]">'mods:detail' darf nur einmal wiederholt werden.</report>
			<!-- 11.3.2 number -->
			<report test="mods:detail/mods:number[2]">'mods:number' darf nicht wiederholt
				werden.</report>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:part[not(@type='constituent')]">
			<assert test="mods:detail/mods:number">'mods:detail/mods:number' ist in 'mods:part'
				nicht vorhanden</assert>
		</rule>
	</pattern>
	<!-- Pflichtelemente in der Wurzelstruktur eines mets-Satzes: recordInfo -->
	<pattern id="Wurzelstrukturelement_recordInfo">
		<rule context="//mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID]">
			<let name="div_dmdid" value="./@DMDID"/>
			<!-- 16.1 recordInfo (Teil 1) -->
			<report subject="ancestor::mets:mets/mets:dmdSec[@ID=$div_dmdid]//mods:mods"
				test="(./@TYPE='Volume' or ./@TYPE='volume' or ./@TYPE='Monograph' or ./@TYPE='monograph') and not(//mets:dmdSec[@ID=$div_dmdid]//mods:mods/mods:recordInfo)"
				>'mods:recordInfo' muss in der bibliographischen Beschreibung von Monographien und
				Bänden enthalten sein (z. B. zum Referenzieren).</report>
		</rule>
	</pattern>
	<!-- SatzID -->
	<pattern id="SatzID">
		<!-- 11.3.4 & 16.1 recordInfo -->
		<rule context="mods:recordInfo">
			<!-- 16.2.1 recordIdentifier -->
			<assert test="mods:recordIdentifier">mods:recordIdentifier fehlt</assert>
			<report test="mods:recordIdentifier[2]">mods:recordIdentifier darf nicht wiederholt
				werden</report>
		</rule>
		<rule context="mods:recordInfo/mods:recordIdentifier">
			<report test="contains(.,' ')">mods:recordIdentifier enthaelt Leerzeichen</report>
		</rule>
	</pattern>
	<!-- Objekt-Identifikator -->
	<pattern id="identifier">
		<!-- 12.1 identifier (Objekt-ID) -->
		<rule context="mods:identifier">
			<assert test="./@type">'mods:identifier' erfordert zwingend ein type-Attribut</assert>
		</rule>
	</pattern>
	<!-- Zugang zur Ressource -->
	<pattern id="location">
		<!-- 13.2.1 physicalLocation -->
		<rule context="mods:location[not(mods:shelfLocator)]">
			<assert test="mods:physicalLocation or mods:url">'mods:location' muss wenigstens
				entweder 'mods:physicalLocation' oder 'mods:url' aufweisen.</assert>
		</rule>
	</pattern>
	<pattern>
		<!-- 13.2.1 physicalLocation -->
		<rule context="mods:location">
			<report test="mods:physicalLocation[2]">'mods:physicalLocation' darf nicht wiederholt
				werden.</report>
			<report test="mods:shelfLocator[2]">'mods:shelfLocator' darf nicht wiederholt
				werden.</report>
		</rule>
	</pattern>
	<pattern>
		<rule context="mods:location[mods:shelfLocator]">
			<assert test="./mods:physicalLocation"> Der Besitzer des Original (Drucks)
				'mods:physicalLocation' und eine Signatur 'mods:shelfLocator' müssen zusammen in
				'mods:location' vorkommen.</assert>
		</rule>
	</pattern>
	<!-- X.1 Leere Elemente -->
	<pattern id="emptyElements">
		<rule context="//mods:*">
			<report test="not(./child::*) and normalize-space(.)=''">'<name/>' hat keinen Inhalt.
				Jedes Element muss ein Textelement oder ein Kindelement mit einem Textelement
				enthalten.</report>
		</rule>
	</pattern>
	<pattern id="mets_structure_types">
		<!-- X.4 METS Strukturtypen -->
		<!-- 	<rule context="//mets:structMap[@TYPE='LOGICAL']//mets:div/@TYPE">
			<assert
				test="document('mets_structure_types.xml')//*[structure_type = normalize-space(current())]"
				>Warnung: 'mets:div' hat einen TYPE-Attributwert '<value-of select="."/>', der
				keinem Strukturdatentyp entspricht, der im DFG-Viewer oder im ZVDD aufgelistet
				wird.</assert>
		</rule>-->
	</pattern>
	<pattern>
		<!-- X.5 METS Strukturtypen monograph und volume - AUSKOMMENTIERT weil es falsch positive Fehlermeldungen bei der 
		OÖ LB Linz bei Sätzen ausgibt, die wie Anchorsätze aussehen, obwohl sie über OAI kommen und von Goobi produziert 
		wurden -->
		<!-- 	<rule
			context="mets:mets/mets:structMap[@TYPE='LOGICAL']//mets:div[@TYPE='Volume' or @TYPE='volume' or @TYPE='Monograph' or @TYPE='monograph' or @TYPE='article']">
			<assert test="./@DMDID"> mets:div (in der logischen structMap) muss eine DMDID
				aufweisen, wenn es mit dem Strukturdatentyp '<value-of select="./@TYPE"/>'
				charakterisiert wird.</assert>
		</rule>-->
	</pattern>
</schema>
