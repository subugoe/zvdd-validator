# METS Validator für [ZVDD](http://www.zvdd.de/)

Dieser METS Validator beruht auf einem Schematron Dokument, das als XSL exportiert wurde. So kann die Validation über das TYPO3 CMS im Web zur Verfügung gestellt werden.


## Einbindung in TYPO3

Das Stylesheet wird über die [xmlinclude](ssp/typo3include) Extension in TYPO3 eingebunden.

Konfigurationsschritte:

* xmlinclude Extension in Version ≥ 2.0.0 installieren
* RealURL für xmlinclude mit der richtigen Seiten ID [konfigurieren](https://github.com/ssp/typo3-xmlinclude#RealURL)
* dieses Repository als Ordner `fileadmin/zvdd-validator` Klonen
* Content Element mit Plugin »Include XML« ind die Seite einfügen
* die Felder im FlexForm für das Content Element leer lassen
* Das `xmlinclude` Template für die Seite hinzufügen
* Im Template diese TypoScript hinterlegen:

		plugin.tx_xmlinclude.settings {
			XSL {
				10 = fileadmin/zvdd-validator/zvdd-validator-load.xsl
				20 = fileadmin/zvdd-validator/schematron/zvdd-validator.xsl
				30 = fileadmin/zvdd-validator/schematron/Darstellung-nur-svrl-text-und-path.xsl
				40 = fileadmin/zvdd-validator/zvdd-validator-form.xsl
				50 = EXT:xmlinclude/Resources/Private/XSL/rewrite-urls.xsl
			}
			headCSS.1 = fileadmin/zvdd-validator/zvdd-validator.css
			headJavaScript.1 = fileadmin/zvdd-validator/zvdd-validator.js
		}


## XSL Schritte

* `zvdd-validator-load.xsl` wertet die Argumente des Aufrufs aus und lädt damit das übergebene Dokument. Es kann von einer URL geladen werden, als Text eingefügt werden und als Datei hochgeladen werden
* `schematron/zvdd-validator.xsl` macht die eigentliche Validation und erzeugt Fehlermeldungen wenn nötig
* `zvdd-validator-form.xsl` sorgt für die Anzeige des Formulars und der Fehlermeldungen


## Weitere Informationen

* [Befehl zum Testen](https://gist.github.com/ssp/5109980) des XSL Ablaufs für Laden von URL im Terminal
* Die Eingabe von XML im Textfeld wird ermöglicht durch die Verfügbarmachen der PHP XML Parsing Funktion als Funktion in XSL mit [http://php.net/manual/de/xsltprocessor.registerphpfunctions.php](registerPHPFunctions())
	* PHPs XML Parsing [ist](https://bugs.php.net/bug.php?id=35247) [buggy](https://bugs.php.net/bug.php?id=64137): es weigert sich, Elemente mit gemischten einfachen und doppelten Anführungszeichen zu lesen. Ein Workaround hierfür macht diesen Schritt unnötig kompliziert.
* Die Dateien `schmatron/bs_*` sind Beispieldateien

## Erstellt für die [SUB Göttingen](http://www.sub.uni-goettingen.de)

* TYPO3 Einbindung, Formular: Sven-S. Porst <porst@sub.uni-goettingen.de>
* Validation, Schematron: Gerrit Kühle

