<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:svrl="http://purl.oclc.org/dsdl/svrl" xmlns="http://www.w3.org/1999/xhtml" version="1.0">

    <xsl:output indent="yes"/>

    <xsl:template match="/">
        <html>
            <head/>
            <body>
                <p class="note">
                    <xsl:text>In der validierten XML-Datei konnten </xsl:text>
                    <xsl:value-of select="count(//svrl:text)"/>
                    <xsl:text> Mal Abweichungen zum MODS AP 2.0 festgestellt werden. Die folgende Liste enthält die unterschiedlichen Abweichungen mit den Pfaden, um die richtige Stelle im Dokument zu finden.</xsl:text>
                    <br/>
                    <!-- <xsl:for-each select="count(//svrl:text[. = 1])">
                        <xsl:text>Es handelt sich um die folgende Abweichung: </xsl:text>
                    </xsl:for-each>
                    <xsl:for-each select="count(//svrl:text[. &gt; 1])"> -->
                </p>
                <xsl:apply-templates select="//svrl:text" mode="b"/>
            </body>
        </html>
    </xsl:template>
    <!-- 
    <xsl:template match="svrl:text" mode="a">
        <xsl:variable name="text" select="."/>

        <xsl:value-of select="count(self::*[not(following-sibling::svrl:text[. = $text])])"/>
    </xsl:template> -->

    <xsl:template match="//svrl:text" mode="b">
        <span class="pathContainer">
            <xsl:for-each select="parent::svrl:*">
                <xsl:sort select="svrl:text"/>
                <xsl:variable name="lastText" select="svrl:text"/>
                <xsl:if test="not(preceding-sibling::svrl:*[svrl:text=$lastText])">
                    <!-- <xsl:if test="parent::svrl:*/preceding-sibling::svrl:fired-rule[1][not(@role)]"> -->
                    <xsl:if test="self::svrl:failed-assert">
                        <ul>
                            <li>
                                <span class="path">
                                    <xsl:text>not(</xsl:text>
                                    <xsl:value-of
                                        select="preceding-sibling::svrl:fired-rule[1]/@context"/>
                                    <xsl:text>/</xsl:text>
                                    <xsl:value-of select="@test"/>
                                    <xsl:text>)</xsl:text>
                                </span>
                                <span class="message">
                                    <xsl:value-of select="svrl:text"/>
                                    <!--
                                    <xsl:text> - Anzahl der Vorkommnisse in der Datei: </xsl:text>
                                    <xsl:value-of
                                        select="count(preceding-sibling::svrl:*[svrl:text=$lastText])"
                                    /> -->
                                </span>
                            </li>
                        </ul>
                    </xsl:if>
                    <xsl:if test="self::svrl:successful-report">
                        <ul>
                            <li>
                                <span class="path">
                                    <xsl:value-of
                                        select="preceding-sibling::svrl:fired-rule[1]/@context"/>
                                    <xsl:text>[</xsl:text>
                                    <xsl:value-of select="@test"/>
                                    <xsl:text>]</xsl:text>
                                </span>
                                <span class="message">
                                    <xsl:value-of select="svrl:text"/>
                                    <!-- 
                                    <xsl:text> - Anzahl der Vorkommnisse: </xsl:text>
                                    <xsl:value-of
                                        select="count(number(svrl:text[.=ancestor::*//svrl:text]))"
                                    /> -->
                                </span>
                            </li>
                        </ul>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>
        </span>
    </xsl:template>
</xsl:stylesheet>
